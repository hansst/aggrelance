#!/bin/bash
# Reset coverage measurment
coverage erase
# Run tests
coverage run manage.py test
# Craete report with which lines where indeed tested.
coverage annotate -d coverage/
cd coverage/
#Remove some noise
rm devenv_lib_python3_6_site-packages_*
rm projects_migrations_*
rm home_*
rm payment_*
rm user_*
#Open the report we are interested in Visual studios code. If you are using a different editor please replace.
code projects_views.py,cover 