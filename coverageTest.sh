#!/bin/bash
# Reset coverage measurment
coverage erase
# Run tests
coverage run manage.py test
# Craete report with which lines where indeed tested.
coverage annotate -d coverage/
cd coverage/
#Remove some noise
rm devenv_lib_python3_6_site-packages_*
rm projects_migrations_*
rm home_*
rm user_*
rm payment_*

#   Open the report we are interested in
#
#   Lines beginning with > are tested
#   Lines beginning with ! are not executed
#   Lines beginning with - are excluded from coverage statistic
#
#

code projects_views.py,cover 