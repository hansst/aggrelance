"""Agreelance User forms

This module gives the forms used for User handling in Agreelance.

It contains the following class
    * SigUpForm - which defines the fields used when signing up a new user.
"""
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from projects.models import ProjectCategory


class SignUpForm(UserCreationForm):
    """Define values to be collected and stored in sigup page"""
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    company = forms.CharField(max_length=30, required=False, help_text= 'Here you can add your company.')
    
    email = forms.EmailField(max_length=254, help_text='Inform a valid email address.')
    email_confirmation = forms.EmailField(max_length=254, help_text='Enter the same email as before, for verification.')
    
    phone_number = forms.CharField(max_length=50)

    country = forms.CharField(max_length=50)
    state = forms.CharField(max_length=50)
    city = forms.CharField(max_length=50)
    postal_code = forms.CharField(max_length=50)
    street_address = forms.CharField(max_length=50)
    queryset = ProjectCategory.objects.all()
    help_text = 'Hold down "Control", or "Command" on a Mac, to select more than one.'
    categories = forms.ModelMultipleChoiceField(queryset=queryset, help_text=help_text)

    class Meta:
        """Defines fields to be collected and stored in User objects"""
        model = User
        fields = ('username', 'first_name', 'last_name', 'categories', 'company' , 'email', 'email_confirmation', 'password1', 'password2', 'phone_number', 'country', 'state', 'city', 'postal_code', 'street_address')
