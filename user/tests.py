from django.test import TestCase
from django.urls import reverse
from projects.tests import create_category
from .forms import SignUpForm
# Create your tests here.

class user_urls_tests(TestCase):
    def setUp(self):
        self.testUrl="/user/signup/"
        self.category1=create_category("Cleaning")
        self.category2=create_category("Painting")
        self.category3=create_category("Gardening")

    
    def test_reverse(self):
        """ Verify that reistering a new user results in redirect to index page """
        signUpform={'username':'joe', 'first_name':'firstJoe', 'last_name':'Doe', 'categories': 1, 'company':'EvilCorp' , 'email':'Joe.Doh@email.email', 'email_confirmation':'Joe.Doh@email.email', 'password1':'testpassword123!', 'password2': 'testpassword123!', 'phone_number':'98765432', 'country':'Norway', 'state':'Oslo', 'city':'Oslo', 'postal_code':'0234', 'street_address':'Karl Johan'}
        response = self.client.post(self.testUrl,data=signUpform)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url,'/')

    
    def test_urlpatters_imports(self):
        """Test that reordering imports does not break correct functioning of all urls given"""
        signUpform={'username':'harry', 'first_name':'harry', 'last_name':'hole', 'categories': 1, 'company':'EvilCorp' , 'email':'Harry.Hole@email.email', 'email_confirmation':'Harry.Hole@email.email', 'password1':'testpassword123!', 'password2': 'testpassword123!', 'phone_number':'98765432', 'country':'Norway', 'state':'Oslo', 'city':'Oslo', 'postal_code':'0234', 'street_address':'Karl Johan'}
        response = self.client.post(self.testUrl,data=signUpform)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url,'/')
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse('login'),data={'username':'harry', 'password1':'testpassword123!'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.url,'/')
        self.assertEqual(response.status_code, 302)

class user_forms_tests(TestCase):

    def test_signupform(self):
        """ Verify that help text is properly displayed on sigup page """
        response = self.client.get(reverse('signup'))
        self.assertContains(response,'Hold down "Control", or "Command" on a Mac, to select more than one.')



