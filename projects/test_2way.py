from django.test import TestCase
from django.test.client import RequestFactory
from projects.views import project_view, get_user_task_permissions
from projects.models import Project, Task, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from user.forms import SignUpForm

from projects.tests import create_user, create_category, create_project, create_taskOffer

#Exercise 2, task 3, two way domain tests
class two_way_domain_test_sign_up(TestCase):

#We want to test input for: 
    def setUp(self):
        self.category=create_category("Cleaning")
        self.category=create_category("Painting")
        self.category=create_category("Gardening")
        self.Username={'Valid': 'testusername', 'Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.first_name={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.last_name={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.categories={'Cleaning':1,'Painting':2,'Gardening':3,'Cleaning and Painting': [1,2],'Cleaning and Gardening':[1,3],'Painting and Gardening':[2,3],'Cleaning and Painting and Gardening':[1,2,3]}
        self.company={'Valid':'EvilCorp','empty':'','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.email={'Valid':'testEmail@email.email','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA@AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA.AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.email_confirm={'Equal':'testEmail@email.email'}
        self.password1={'Valid':'ghrueskjhfiowaf1235423','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.password2={'Equal':'ghrueskjhfiowaf1235423'}
        self.phone_number={'Valid':'98765432','Long':'9876543212345678901234567890123456789012345678901234567890'}
        self.country={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.state={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.city={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.postal_code={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.street_address={'Valid':'test','Long':'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'}
        self.testUrl="/user/signup/"
        self.tests='''1,Valid,Valid,Valid,Cleaning,Valid,Valid,Equal,Valid,Equal,Valid,Valid,Valid,Valid,Valid,Valid
2,Valid,,,Painting,empty,,,,,,,,,,
3,Valid,,,Gardening,,,,,,,,,,,
4,Valid,,,Cleaning and Painting,,,,,,,,,,,
5,Valid,,,Cleaning and Gardening,,,,,,,,,,,
6,Valid,,,Painting and Gardening,,,,,,,,,,,
7,Valid,,,Cleaning and Painting and Gardening,,,,,,,,,,,
8,,,,Cleaning and Painting,,,,Valid,,,,,,,Valid
9,,,,Cleaning and Gardening,,,Equal,,,,,,,Valid,
10,,,,Painting and Gardening,,Valid,,,,,,,Valid,,
11,,,,Cleaning and Painting and Gardening,Valid,,,,,,,Valid,,,
12,,,,Cleaning,empty,,,,,,Valid,,,,
13,,,Valid,Painting,,,,,,Valid,,,,,
14,,Valid,,Gardening,,,,,Equal,,,,,,
15,,,,Cleaning and Painting and Gardening,empty,,,Valid,,,,,,,Valid
16,,,,Cleaning,,,,,,,Valid,,,,
17,,,,Painting,,,Equal,,,,,,,Valid,
18,,,Valid,Gardening,,,,,,Valid,,,,,
19,,,,Cleaning and Painting,,Valid,,,,,,,Valid,,
20,,Valid,,Cleaning and Gardening,,,,,Equal,,,,,,
21,,,,Painting and Gardening,Valid,,,,,,,Valid,,,
22,,,,Gardening,,,,Valid,,,,,,,Valid
23,,,Valid,Cleaning and Painting,,,,,,Valid,,,,,
24,,,,Cleaning and Gardening,Valid,,,,,,,Valid,,,
25,,,,Painting and Gardening,empty,,Equal,,,,,,,Valid,
26,,Valid,,Cleaning and Painting and Gardening,,,,,Equal,,,,,,
27,,,,Cleaning,,,,,,,Valid,,,,
28,,,,Painting,,Valid,,,,,,,Valid,,
29,,,,Painting and Gardening,,,,Valid,,,,,,,Valid
30,,,,Cleaning and Painting and Gardening,,Valid,,,,,,,Valid,,
31,,,,Cleaning,,,,,,,Valid,,,,
32,,Valid,,Painting,,,,,Equal,,,,,,
33,,,,Gardening,,,Equal,,,,,,,Valid,
34,,,,Cleaning and Painting,Valid,,,,,,,Valid,,,
35,,,Valid,Cleaning and Gardening,empty,,,,,Valid,,,,,
36,,,,Painting,,,,Valid,,,,,,,Valid
37,,,,Gardening,Valid,,,,,,,Valid,,,
38,,Valid,,Cleaning and Painting,empty,,,,Equal,,,,,,
39,,,,Cleaning and Gardening,,Valid,,,,,,,Valid,,
40,,,Valid,Painting and Gardening,,,,,,Valid,,,,,
41,,,,Cleaning and Painting and Gardening,,,Equal,,,,,,,Valid,
42,,,,Cleaning,,,,,,,Valid,,,,
43,,,,Cleaning and Gardening,,,,Valid,,,,,,,Valid
44,,Valid,,Painting and Gardening,,,,,Equal,,,,,,
45,,,Valid,Cleaning and Painting and Gardening,,,,,,Valid,,,,,
46,,,,Cleaning,,,,,,,Valid,,,,
47,,,,Painting,Valid,,,,,,,Valid,,,
48,,,,Gardening,empty,Valid,,,,,,,Valid,,
49,,,,Cleaning and Painting,,,Equal,,,,,,,Valid,'''
        self.tests=self.tests.split('\n')
        self.tests.reverse()

        
    def Test_two_way_domain_tests(self,signUpform):
        users=User.objects.all()
        if User.objects.filter(username='testusername').exists():
            user=User.objects.get(username ='testusername')
            user.delete()
        response = self.client.post(self.testUrl,data=signUpform)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url,'/')
     
    def test_runner(self):
        for i in range(49):
            test=self.tests[i].split(',')
            for j in range(len(test)):
                if (j==7 or j==9 ) and test[j]=='':
                    test[j]='Equal'
                if test[j]=='':
                    test[j]='Valid'
            signUpform={'username':self.Username[test[1]], 
            'first_name':self.first_name[test[2]], 
            'last_name':self.last_name[test[3]], 
            'categories': self.categories[test[4]], 
            'company':self.company[test[5]], 
            'email':self.email[test[6]], 
            'email_confirmation':self.email_confirm[test[7]], 
            'password1':self.password1[test[8]], 
            'password2': self.password2[test[9]], 
            'phone_number':self.phone_number[test[10]], 
            'country':self.country[test[11]], 
            'state':self.state[test[12]], 
            'city':self.city[test[13]], 
            'postal_code':self.postal_code[test[14]], 
            'street_address':self.street_address[test[15]]}
            self.Test_two_way_domain_tests(signUpform)
