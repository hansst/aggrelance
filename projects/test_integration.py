from django.test import TestCase
from django.test.client import RequestFactory
from projects.views import project_view, get_user_task_permissions
from projects.models import Project, Task, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from user.forms import SignUpForm

from projects.tests import create_user, create_category, create_project, create_taskOffer


#Exercise 2, task 3, integration tests
class unit_integration_tests_new_features(TestCase):
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.client.login(username='test', password='test_password')
        proj1_data={'title': 'Project_1', 'description':'Desc_project_1', 'city':'Trondheim', 'zip_code':'7777', 'country':'Norway','category_id': 1}
        response = self.client.post('/projects/new/',data=proj1_data)
        proj2_data={'title': 'Project_2', 'description':'Desc_project_2', 'city':'', 'zip_code':'', 'country':'','category_id': 1}
        response = self.client.post('/projects/new/',data=proj2_data)
        self.client.logout()
        self.testUrl="/projects/"
        self.testUrlProj1="/projects/1/"
        self.testUrlProj2="/projects/2/"

    def Test_projects_page(self):
        response = self.client.get(self.testUrl)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,"Trondheim")
        self.assertContains(response,"Not specified")

    def Test_projects_location_Paris(self):
        response = self.client.get(self.testUrlProj1)
        self.assertContains(response,"Trondheim")
    
    def Test_projects_location_NotSet(self):
        response = self.client.get(self.testUrlProj2)
        self.assertContains(response,"Project location: Not specified")
    
                
    def test_runner(self):
        self.Test_projects_page()
        self.Test_projects_location_Paris()
        self.Test_projects_location_NotSet()
