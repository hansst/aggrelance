from django.test import TestCase
from django.test.client import RequestFactory
from projects.views import project_view, get_user_task_permissions
from projects.models import Project, Task, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from user.forms import SignUpForm

def create_user(username, password, company):
    user = User.objects.create_user(username=username)
    user.set_password(password)
    user.save()
    user.refresh_from_db()
    user.is_active = True
    user.profile.company = company
    user.save()
    return user

def create_category(category):
    return ProjectCategory.objects.create(name=category)

def create_project(project_user,project_title,project_description,project_category,task_list,location):
    project = Project.objects.create(user=project_user.profile, title=project_title, description=project_description, category=project_category, location=location)
    for task in task_list:
        newTask=Task.objects.create(project=project, title=task['title'], description=task['desc'], budget=task['budget'])
    return project

def create_taskOffer(task,title,description,price,offerer,status):
    return TaskOffer.objects.create(task=task,title=title,description=description,price=price,offerer=offerer.profile,status=status)

class project_view_refactor(TestCase):
    def test_project_view_get_request(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Washington')
        self.testUrl="/projects/"+str(self.project.id)+"/"
        response = self.client.get(self.testUrl)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,"Washington")



class model_taskoffer_refactor_tests(TestCase):  
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.secondOfferuser=create_user('offeruser2','offeruser_password2','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Washington')
        self.testUrl="/projects/"+str(self.project.id)+"/"

    def test_create_project_give_offer_and_accept_offer(self):
        self.client.login(username='test', password='test_password')
        response = self.client.get(self.testUrl)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='offeruser', password='offeruser_password')
        offerdata={'title': ['Testtask1'], 'description': ['This is the offer description'], 'price': [50], 'taskvalue': ['1'], 'offer_submit': ['']}
        response = self.client.post(self.testUrl,data=offerdata)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='offeruser2', password='offeruser_password2')
        offerdata={'title': ['Testtask1'], 'description': ['This is the offer description'], 'price': [500], 'taskvalue': ['1'], 'offer_submit': ['']}
        response = self.client.post(self.testUrl,data=offerdata)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='test', password='test_password')
        offerRespData = {'status': ['a'], 'feedback': ['Task offer accepted'], 'taskofferid': [1], 'offer_response': ['']}
        response = self.client.post(self.testUrl,data=offerRespData)    
        self.assertEqual(response.status_code, 200)
        self.client.logout()


class verify_task_permissions_after_refactor(TestCase):
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.otheruser=create_user('testtwo','testtwo_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Sacramento')
        self.first_task = self.project.tasks.all()[0]
        self.taskOffer=create_taskOffer(self.first_task,'Test offer','Description of the test offer',20,self.offeruser,'a')
        
    def test_get_user_task_permission_project_user(self):
        result = get_user_task_permissions(self.user, self.first_task)
        self.assertEqual(result['owner'], True)

        result = get_user_task_permissions(self.offeruser, self.first_task)
        self.assertEqual(result['write'], True)
        self.assertEqual(result['read'], True)
        self.assertEqual(result['modify'], True)
        self.assertEqual(result['owner'], False)
        self.assertEqual(result['upload'], True)

        result = get_user_task_permissions(self.otheruser, self.first_task)
        self.assertEqual(result['write'], False)
        self.assertEqual(result['read'], False)
        self.assertEqual(result['modify'], False)
        self.assertEqual(result['owner'], False)
        self.assertEqual(result['upload'], False)

class project_view_refactoring_tests(TestCase):
    
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Washington')
        self.testUrl="/projects/"+str(self.project.id)+"/"

    def test_project_view_after_refactor(self):
        self.client.login(username='test', password='test_password')
        response = self.client.get(self.testUrl)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='offeruser', password='offeruser_password')
        offerdata={'title': ['Testtask1'], 'description': ['This is the offer description'], 'price': [50], 'taskvalue': ['1'], 'offer_submit': ['']}
        response = self.client.post(self.testUrl,data=offerdata)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        self.client.login(username='test', password='test_password')
        offerRespData = {'status': ['a'], 'feedback': ['Task offer accepted'], 'taskofferid': [1], 'offer_response': ['']}
        response = self.client.post(self.testUrl,data=offerRespData)    
        self.assertEqual(response.status_code, 200)

        projectStatusChangeData = {'status': ['i'], 'status_change': ['']}
        response = self.client.post(self.testUrl, data=projectStatusChangeData)
        self.assertEqual(response.status_code, 200)
