from django.test import TestCase
from django.test.client import RequestFactory
from projects.views import project_view, get_user_task_permissions
from projects.models import Project, Task, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from user.forms import SignUpForm

from projects.tests import create_user, create_category, create_project, create_taskOffer

#Exercise 2, task 3, output tests
class output_tests_accepting_offers(TestCase):   
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Washington')
        self.testUrl="/projects/"+str(self.project.id)+"/"
        self.client.login(username='offeruser', password='offeruser_password')
        offerdata={'title': ['Testtask1'], 'description': ['This is the offer description'], 'price': [50], 'taskvalue': ['1'], 'offer_submit': ['']}
        response = self.client.post(self.testUrl,data=offerdata)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def Test_project_view_offer_accept(self):
        self.client.login(username='test', password='test_password')
        offerRespData = {'status': ['a'], 'feedback': ['Task offer accepted'], 'taskofferid': [1], 'offer_response': ['']}
        response = self.client.post(self.testUrl,data=offerRespData)    
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,'Accepted')
        self.client.logout()

    def Test_project_view_(self):
        self.client.login(username='test', password='test_password')
        offerRespData = {'status': ['a'], 'feedback': ['Task offer accepted'], 'taskofferid': [1], 'offer_response': ['']}
        response = self.client.post(self.testUrl,data=offerRespData)    
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,'Accepted')
        self.assertContains(response,'Offer from offeruser')
        self.client.logout()

    def test_runner_project_view(self):
        self.Test_project_view_offer_accept()
