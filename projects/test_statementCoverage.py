from django.test import TestCase
from django.test.client import RequestFactory
from projects.views import project_view, get_user_task_permissions
from projects.models import Project, Task, ProjectCategory, TaskOffer
from user.models import Profile
from django.contrib.auth.models import User
from user.forms import SignUpForm

from projects.tests import create_user, create_category, create_project, create_taskOffer

#Exercise 2, task 2, full statement coverage project_view
class project_view_Tests(TestCase):
    
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Washington')
        self.testUrl="/projects/"+str(self.project.id)+"/"

    def Test_project_view_get_request(self):
        self.client.login(username='test', password='test_password')
        response = self.client.get(self.testUrl)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def Test_project_view_post_offer_submit(self):
        self.client.login(username='offeruser', password='offeruser_password')
        offerdata={'title': ['Testtask1'], 'description': ['This is the offer description'], 'price': [50], 'taskvalue': ['1'], 'offer_submit': ['']}
        response = self.client.post(self.testUrl,data=offerdata)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def Test_project_view_post_offer_response(self):
        self.client.login(username='test', password='test_password')
        offerRespData = {'status': ['a'], 'feedback': ['Task offer accepted'], 'taskofferid': [1], 'offer_response': ['']}
        response = self.client.post(self.testUrl,data=offerRespData)    
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def Test_project_view_post_status_change(self):
        self.client.login(username='test', password='test_password')
        projectStatusChangeData = {'status': ['i'], 'status_change': ['']}
        response = self.client.post(self.testUrl, data=projectStatusChangeData)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

    def test_runner_project_view(self):
        self.Test_project_view_get_request()
        self.Test_project_view_post_offer_submit()
        self.Test_project_view_post_offer_response()
        self.Test_project_view_post_status_change()

#Exercise 2, task 2, full statement coverage get user task permissions
class get_user_task_permissions_TestCase(TestCase):
    def setUp(self):
        self.user=create_user('test','test_password','EvilCorp')
        self.offeruser=create_user('offeruser','offeruser_password','EvilCorp')
        self.otheruser=create_user('testtwo','testtwo_password','EvilCorp')
        self.category=create_category("Cleaning")
        task_list=[
            {'title': "Testtask1", 'desc': "This is test projects 1's first testtask", 'budget': 100},
            {'title': "Testtask2", 'desc': "This is test projects 2's first testtask", 'budget': 200}
        ]
        self.project=create_project(self.user,"Test_title","Test_description_field",self.category,task_list,'Sacramento')
        self.first_task = self.project.tasks.all()[0]
        self.taskOffer=create_taskOffer(self.first_task,'Test offer','Description of the test offer',20,self.offeruser,'a')
        
    def Test_get_user_task_permission_project_user(self):
        result = get_user_task_permissions(self.user, self.first_task)
        self.assertEqual(result['owner'], True)

    def Test_get_user_task_permissions_project_accepted_offerer(self):
        result = get_user_task_permissions(self.offeruser, self.first_task)
        self.assertEqual(result['write'], True)
        self.assertEqual(result['read'], True)
        self.assertEqual(result['modify'], True)
        self.assertEqual(result['owner'], False)
        self.assertEqual(result['upload'], True)

    def Test_get_user_task_permissions_project_random_user(self):
        result = get_user_task_permissions(self.otheruser, self.first_task)
        self.assertEqual(result['write'], False)
        self.assertEqual(result['read'], False)
        self.assertEqual(result['modify'], False)
        self.assertEqual(result['owner'], False)
        self.assertEqual(result['upload'], False)

    def test_runner_get_user_task_permissions(self):
        self.Test_get_user_task_permission_project_user()
        self.Test_get_user_task_permissions_project_accepted_offerer()
        self.Test_get_user_task_permissions_project_random_user()
